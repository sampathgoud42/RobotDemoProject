*** Settings ***
Resource  ../../keywords/test_keywords.robot
Test Setup  Test Case Setup
Test Teardown  Test Case Teardown

*** Variables ***
${userName}  testss11@gmail.com
${password}  12345
${resolution_w}  ${640}
${resolution_h}  ${1136}


*** Keywords ***
Test Case Setup
    Initiate

Test Case Teardown
    Close All Browsers

Click On Login.Button
    Click Element Max  css=${signin_css}
    

    # Login  ${EMAIL_INPUT_CSS}=${username}  ${PASSWORD_INPUT_CSS}=${password}  ${SUBMIT_LOGIN_CSS}=${submit_action}..${SUCCESS_LOGIN_MESSAGE_CSS}=${login_success_message}

***Test Cases ***
Test 001 - Verify User Is Able To Login Successfully 
    Import Variables  ${base_dir}\\variables\\ecom\\var.py
    Go To  ${APP_URL}
    Click On Login.Button
    Enter UserName And Password  ${userName}  ${password}
    Click On Login Submit
    Verify the Successful Login




