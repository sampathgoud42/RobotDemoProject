*** Keywords ***
Initiate
    [Arguments]  ${browser}=Chrome
    Create Web Driver  ${browser}
    Maximize Browser Window

Setup Resolution
    [Arguments]  ${width}  ${height}
    Set Window Size  ${width}  ${height}
    log  Set the window width to ${width}
    log  Set the window height to ${height}

Clear And Input Text
    [Arguments]  ${the_css}  ${the_value}
    Clear Element Text  css=${the_css}
    Input Text  css=${the_css}  ${the_value}

Enter UserName And Password
    [Arguments]  ${username}=${username}  ${password}=${password}
    Clear And Input Text  ${EMAIL_INPUT_CSS}  ${username}
    Clear And Input Text  ${PASSWORD_INPUT_CSS}  ${password}
    Capture Page Screenshot

Click On Login Submit
 #   [Arguments]    ${SUBMIT_LOGIN_CSS}=${SUBMIT_LOGIN_CSS}
 #   Submit Form  ${SUBMIT_FORM}
     Click Element  id = ${SUBMIT_LOGIN_ID}

Verify the Successful Login
	Capture Page Screenshot
  	Wait Until Element Is Visible  css=${SUCCESS_LOGIN_MESSAGE_CSS}  ${pause_for_post}
    Page Should Contain  ${SUCCESS_LOGIN_MESSAGE}
    

# Login
#     [Arguments]  ${username}=${username}  ${password}=${password}  ${submit_action}=${submit_action}  ${login_success_message}=${login_success_message}
#     Go To  ${app_url}
#     Clear And Input Text  ${username_css}  ${username}
#     Clear And Input Text  ${password_css}  ${password}
#     Submit Form  ${submit_action}
#     Capture Page Screenshot
#     Page Should Contain  ${login_success_message}

Wait And Input Text
    [Arguments]  ${css}  ${value}  ${sleep_time}=${pause_for_get}
    Wait Until Element Is Visible  css=${css}  ${sleep_time}
    Clear And Input Text  ${css}  ${value}

Generate Account
    ${letters}=  Generate Random String  6  [LETTERS]
    ${numbers}=  Generate Random String  6  [NUMBERS]
    Set Suite Variable  ${account}  ${letters}${numbers}
    [Return]  ${account}

Click Element Max
    [Arguments]  ${css}
    Maximize Browser Window
    Wait Until Element Is Visible  css=${css}  ${pause_for_get}
    Focus  css=${css}
    Click Element  css=${css}

Click And Input Text
    [Arguments]  ${css}  ${value}
    Click Element  css=${css}
    Input Text  css=${css}  ${value}