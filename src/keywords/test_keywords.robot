*** Settings ***
Library  Selenium2Library
Library  String
Resource  base.robot


*** Variables ***
${base_dir}  D:\\myWorkSpace\\RobotDemoProject\\src
${pause_for_get}  10s
${pause_for_post}  30s
${general_retry_duration}  30s
${general_retry_interval}  1s