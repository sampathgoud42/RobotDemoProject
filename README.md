    
How to setup basic environment for robot framework

Download Python for windows from below link
https://www.python.org/ftp/python/2.7.12/python-2.7.12.msi

Install Python 
Install python-2.7.12.amd64.msi
Add C:\Python27;C:\Python27\Scripts into PATH


Install robotframework
Open cmd input>> pip install robotframework
Verified installation, cmd >> robot --version, should see the latest version 
Open cmd input>>pip install robotframework-selenium2library
pip install robotframework-databaselibrary
pip install requests
pip install lxml
pip install libxml2
pip install pymssql
pip install cx_Oracle
pip install MySQL-python


******Using Eclipse******
Install below plugins from Eclipse market place
1) PyDev for Eclipse
2) RobotFramework for Eclipse IDE
3) Create a project (import file system for existing project)


***** Using Sublime*****
Install Sublime text3 wih robotframework plug-in 
Install Sublime Text Build 3124 x64 Setup.exe
Open sublime3 > view > show console
Paste the grey part into the console
import urllib.request,os,hashlib; h = '261dd1222b4693ce6d4f85f9c827ac06' + '6d5ab8ebdd020086947172a8a1356bb6'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
In sublime3 > View > hide console
In sublime3 > preference control > packag control:install package > input Robot Framework Assistant


***** Browser related ****** 
phantomjs is no need to be installed but in phantomjs-2.1.1-windows
chromedriver.exe is a separate executable that WebDriver uses to control Chrome
Make sure copy of Chrome and IE driver files are present in Python Directory (eg: C:\python27\chromedriver.exe)

Commands for execute robot framework test scripts:
-V , to import the python format variable file
    eg. -V var.py
-v, to specify the parameter 
    eg. -v username:michael.lee
-i, to specify the tag to execute
    eg. -i test
-e, to exclude the tag, and run the rest.
    eg. -e test
-d, to specify the output dir.
    eg. -d C:\Rwbot\outputs
    
So, the most coomon execute command will look like this:
    pybot -i test -V var.py -v username:michael.lee test.robot
    
    
****How To execute the project*****

pybot -V D:\myWorkSpace\RobotDemoProject\src\variables\ecom\var.py D:\myWorkSpace\RobotDemoProject\src\test_cases\ecom\test.robot

Note: Make sure path of "var.py" and "test.robot" files are correct.

